/*
# input-cmi.c: routines to control the CMI parsing.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/

#include "config.h"
#include "libfile.h"
#include "input-cmi.h"

/* Font-specific sidebearing and other information.  (-cmi-files)
   `cmi_file' is used as the input file in `cmi.y'.  */
FILE *cmi_file;
string *cmi_names = NULL;

/* Call `read_cmi_file' on each CMI file in CMI_NAMES.  DPI is
   simply passed along.  */

void
read_cmi_file_list (string *cmi_names, string dpi)
{
  if (cmi_names == NULL)
    return;

  for ( ; *cmi_names != NULL; cmi_names++)
    {
      read_cmi_file (*cmi_names, dpi);
    }
}


/* Read the CMI file `NAME.DPIcmi', unless NAME has a suffix, in which
   case don't append the `.DPIcmi'.  This uses the global `cmi_file' to
   communicate with the Bison parser.  */

void
read_cmi_file (string name, string dpi)
{
  extern int yyparse ();

  cmi_file = libfile_start (name, concat (dpi, "cmi"));
  yyparse ();

  libfile_close ();
}
