/*
# output.h: declarations for outputting the newly spaced font.
# 
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/

#ifndef OUTPUT_H
#define OUTPUT_H

#include "font.h"
#include "char.h"


/* See output.c.  */
extern string fontdimens;
extern string output_name;
extern charcode_type xheight_char;

/* Output a TFM and (perhaps) GF file with the new spacings for the
   characters CHARS.  Use the font B for the character bitmaps.  The
   font is written to the current directory.  */
extern void output_font (bitmap_font_type b, char_type *chars[]);

#endif /* not OUTPUT_H */
