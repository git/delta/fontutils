define redo
symbol-file spectrum
exec-file spectrum
end

directory ../lib
directory ../gf
directory ../pk
directory ../tfm

# Signature stuff.
#set args -no-second -no-variance -dump 3 pnr10 -verbose -window blackman \
-dump-text $dtrg/TeX/ram.txt -dumpdir karltest \
-f-bandpass 00 04 -f-bandpass 04 08 -f-bandpass 08 12 \
         -f-bandpass 12 16 -f-bandpass 16 20 -f-bandpass 20 24 \
         -f-bandpass 24 28 -f-bandpass 28 32 -f-bandpass 32 36 \
         -f-highpass 36    -f-wilson 1 -f-wilson 2 -f-wilson 3 \
	 -f-wilson 4 -f-wilson 5 -f-wilson 6 -f-wilson 7 -f-wilson 8 \
	 -f-wilson 9 -f-wilson 10 -f-wilson 11 -f-wilson 12
#set args -no-second -dump 61 cmr10 -verbose -dumpdir karltest -W 64
#set args -dump 100 Bembo-BoldItalic -no-postscript -W 128 -verbose

# Kathy's artwork.
#set args -W 64 -H 64 -no-second -font cmss10 fuck shit hate love \
  -title "Four four-letter words" -verbose > art.ps

# Karl's talk to IP class, Sept 93.
#set args -W 64 -H 64 -text=m cmr10 cmss10 -verbose -dc-in-mid -title \
"spectra of letters (#1)"> m.ps

# Perfect back transforms
#set args -W 64 -H 64 -font=cmss10 Karl -verbose -back -noise=0 -epsf >foo.ps

# All white.
#set args -W 16 -H 16 -font=cmtex10 " " -verbose -back -noise=0 -epsf >foo.ps

# Some noise, PGM output only.
set args -W 64 -H 64 -font=cmss10 Karl -verbose -back -noise=1 -pgm >/dev/null
