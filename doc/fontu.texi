\input texinfo @c -*-texinfo-*-
@c %**start of header
@setfilename fontu.info
@include version.texi
@settitle GNU Font Utilities @value{VERSION}

@c Define a new index for options.
@defcodeindex op
@c Combine everything into one index (arbitrarily chosen to be the 
@c concept index).
@syncodeindex op cp
@syncodeindex cm cp
@syncodeindex fl cp
@syncodeindex fn cp
@syncodeindex ky cp
@syncodeindex op cp
@syncodeindex pg cp
@syncodeindex vr cp
@c %**end of header

@copying
This manual is for GNU Font Utilities (version @value{VERSION}, @value{UPDATED}), which provides porgrams for font manipulation.

Copyright (C) 1992, 1993, 2004, 2011, 2012 Free Software Foundation, Inc.

@quotation
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts, and with no Back-Cover
Texts.  A copy of the license is included in the section entitled
``GNU Free Documentation License''.
@end quotation
@end copying

@dircategory Typesetting
@direntry
* Font utilities: (fontu).      Programs for font manipulation.
@end direntry

@titlepage
@title GNU Font Utilities
@subtitle for version @value{VERSION}, @value{UPDATED} 
@ignore
@author Karl Berry and Kathryn A. Hargreaves
@end ignore
@author GNU Font Utilities Developers (@email{fontutils-bugs@@gnu.org}) 
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@contents

@ifnottex
@node Top
@top GNU Font Utilities

This manual documents how to install and run the GNU Font Utilities.  It
corresponds to version @value{VERSION} (released in @value{UPDATED}).

The introduction briefly describes the purpose and philosophy of the
Font Utilities.  The overview gives details on their general usage,
especially how they interact, and describes various things which are
common to all or most of the programs.

The first part of this master menu lists the major nodes in this Info
document, including the index.  The rest of the menu lists all the
lower level nodes in the document.

@menu
* Introduction::                A brief introduction.
* Installation::                How to compile and install the Font Utilities.
* Overview::                    Commonalities to the programs,
				  a roadmap to how they fit together,
                                  and examples of usage.
* Reporting bugs::              How, why, and where to report bugs.
* File formats::                These programs read and write many files.
* Imageto::                     Extracting a font from a scanned image.
* IMGrotate::                   Rotating an image.
* Fontconvert::                 Manipulation of bitmap fonts.
* Charspace::                   Adding character metrics to a font.
* Limn::                        Making outlines from bitmaps.
* BZRto::                       Converting generic outlines to other formats.
* BPLtoBZR::                    Converting plain text to binary BZR fonts.
* XBfe::                        Hand editor for bitmap fonts under X11.
* BZRedit::                     Hand editor for outline fonts under Emacs.
* GSrenderfont::                Rasterize PostScript fonts.
* Enhancements::                Future projects.
* GNU Free Documentation License:: Copying and sharing this documentation.
* Concept index::               Index of concepts.

@detailmenu
 --- The Detailed Node Listing ---

Installation

* Prereqs::                     What's needed before installation.
* Problems::                    Known trouble.

Prerequisites

* Archives::                    Where to find programs.

Overview

* Picture::                     A pictorial overview.
* Creating fonts::              How to use the programs together.
* Command-line options::        Many aspects of the command line are
  				  common to all programs.
* Font searching::              How fonts and other files are looked for.
* Font naming::                 How to name fonts.

Creating fonts

* Font creation example::       A real-life example.

Command-line options

* Main input file::             Each program operates on a ``main'' font.
* Options: Common options.      Some options are accepted by all programs.
* Specifying character codes::  Ways of specifying single characters.
* Values: Common option values.  Some options need more information.

Specifying character codes

* Named character codes::       Character names are looked up in the encoding.
* Numeric character codes::     Decimal, octal, hex, or ASCII.

Bugs

* Bug criteria::                Have you found a bug?
* Bug reporting::               How to effectively report a bug.

Bug reporting

* Necessary information::       What you need to send.
* Unnecessary information::     What you don't need to send.
* Documentation bugs::          Report the bugs in the manual, too.

File formats

* File format abbreviations::   The alphabet soup of font formats.
* Common file syntax::          Some elements of auxiliary files are constant.
* Encoding files::              The character code-to-shape mapping.
* Coding scheme map file::      The coding scheme string-to-filename mapping.

Encoding files

* Character names::             How to write character names.
* Ligature definitions::        How to define ligatures.
* GNU encodings::               Why we invented new encodings for GNU.

Imageto

* Imageto usage::               Process for extracting fonts from an image.
* IFI files::                   IFI files supply extra information.
* Invoking Imageto::            Command-line options.

Imageto usage

* Viewing an image::            Seeing what's in an image.
* Image to font conversion::    Extracting a font.
* Dirty images::                Handling scanning artifacts or other noise.

IMGrotate

* IMGrotate usage::             Doing the image rotation.
* Invoking IMGrotate::          Command-line options.

IMGrotate usage

* Clockwise rotation::          Rotating clockwise.
* Flip rotation::               FLipping end-for-end.

Fontconvert

* Invoking Fontconvert::        Command-line options.

Invoking Fontconvert

* Fontconvert output options::  Specifying the output format(s).
* Character selection options::  What characters to operate on.
* Character manipulation options::  Changing characters' appearance.
* Fontwide information options::  Changing global information in a font.
* Miscellaneous options::       Other options.

Charspace

* Charspace usage::             Details on improving the character metrics.
* CMI files::                   You specify the metrics in a separate file.
* Invoking Charspace::          Command-line options.

CMI files

* CMI tokens::                  The building blocks of CMI files.
* char command::                Defining a character's side bearings.
* char-width command::          Defining side bearings via the set width.
* define command::              Introducing a new identifier.
* kern command::                Defining a kerning pair.
* codingscheme command::        Specifying the font encoding.
* fontdimen command::           Defining additional font parameters.
* CMI processing::              How Charspace reads CMI files.

@code{fontdimen} command

* TFM fontdimens::              All the valid fontdimens.

Limn

* Limn algorithm::              How Limn fits outlines to bitmaps.
* Invoking Limn::               Command-line options.

Limn algorithm

* Finding pixel outlines::      Extracting the edges from the bitmap.
* Finding corners::             Finding subsections of each outline.
* Removing knees::              Removing extraneous points.
* Filtering curves::            Smoothing the outlines.
* Fitting the bitmap curve::    Doing the fitting.
* Changing splines to lines::   Use straight lines where possible.
* Changing lines to splines::   Sometimes it isn't possible.
* Aligning endpoints::          If points are close enough, line them out.
* Displaying fitting online::   Seeing the results as Limn runs.

Fitting the bitmap curve

* Initializing t::              Initializing the parameter values.
* Finding tangents::            Computing the direction of the curve at
                                  the endpoints.
* Finding the spline::          Where are the control points?
* Reparameterization::          Changing the parameter values.
* Subdivision::                 Splitting the curve into pieces.

BZRto

* Metafont and BZRto::          Output as a Metafont program.
* Type 1 and BZRto::            Output as a Type 1 PostScript font.
* Type 3 and BZRto::            Output as a Type 3 PostScript font.
* CCC files::                   Creating additional characters.
* Invoking BZRto::              Command-line options.
* BZR files::                   The technical definition of BZR format.

Metafont and BZRto

* Metafont output at any size::  Making larger or smaller fonts.
* Proofing with Metafont::      Metafont can help with debugging fonts.

CCC files

* setchar: CCC setchar.         Statements for including a character.
* move: CCC move.               Statements for moving to a new position.

BZR files

* Intro: BZR format introduction.  General concepts and definitions.
* Preamble: BZR preamble.       The beginning.
* Chars: BZR characters.        The middle.
* Postamble: BZR postamble.     The end.

BZR characters

* BOC: BZR character beginnings.  Giving character metrics.
* Shape: BZR character shapes.  Defining the outline(s).

BPLtoBZR

* BPL files::                   Bezier property list file format.
* Invoking BPLtoBZR::           Command-line options.

BPL files

* Preamble: BPL preamble.       The beginning.
* Characters: BPL characters.   The middle.
* Postamble: BPL postamble.     The end.

BPL characters

* BPL outlines::                Representation of character outlines.

XBfe

* XBfe usage::                  How to edit fonts.
* Invoking XBfe::               Command-line options.

XBfe usage

* Controlling XBfe ::           Controlling XBfe 
* Shape: XBfe shape editing.    Changing the pixels.
* Metrics: XBfe metrics editing.  Changing the side bearings.

XBfe shape editing

* Selections::                  Marking pixel regions for later operations.
* Enlarging the bitmap::        Give yourself more room at the edges.

BZRedit

* BZRedit usage::               Operating the editor.

BZRedit usage

* BZRedit installation::        Additional installation is needed.
* BZR: Editing BZR files.       Editing files in the binary format.
* BPL: Editing BPL files.       Editing files in the textual format.

Editing BPL files

* BZRedit and Ghostscript::     Customizing the use of Ghostscript.

GSrenderfont

* GSrenderfont usage::          Making bitmap fonts from PostScript.
* Invoking GSrenderfont::       Command-line options.

GSrenderfont usage

* Names: GSrenderfont font names.  Supplying PostScript names and filenames.
* Size: GSrenderfont output size.  Specifying the size and resolution.
* Encoding: GSrenderfont encoding.  Specifying the output encoding.

Enhancements

* Additional fonts::            GNU needs more fonts.
* Program features::            These programs can be improved.
* Portability::                 Assumptions about the programming environment.
* Implementation::              Conventions we used in the sources.
@end detailmenu
@end menu
@end ifnottex

@include intro.texi
@include install.texi
@include overview.texi
@include bugs.texi
@include filefmts.texi
@include imageto.texi
@include imgrotate.texi
@include fontcvt.texi
@include charspace.texi
@include limn.texi
@include bzrto.texi
@include bpltobzr.texi
@include xbfe.texi
@include bzredit.texi
@include gsrenderf.texi
@include enhance.texi

@node GNU Free Documentation License
@appendix GNU Free Documentation License

@include fdl.texi

@include index.texi

@bye
