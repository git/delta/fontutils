/*
# output-epsf.h: declarations for writing EPS files.
#
# Copyright (C) 1992 Free Software Foundation, Inc.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/


#ifndef OUTPUT_EPSF_H
#define OUTPUT_EPSF_H

#include <kpathsea/types.h>
#include "font.h"


extern void epsf_start_output (const_string base_name);
extern void epsf_output_char (char_info_type);
extern void epsf_finish_output (void);

#endif /* not OUTPUT_EPSF_H */
