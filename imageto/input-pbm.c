/*
# input-pbm.c: read PBM files.
#
# Copyright (C) 1990, 1991, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/

#include "config.h"

#include "file-input.h"

#include "main.h"
#include "image-header.h"
#include "input-pbm.h"

#include "pbmplus.h"
#include "pbm.h"


/* Where the input comes from.  */
static FILE *pbm_input_file;
static string pbm_input_filename;



/* Only one file can be open at a time.  We do no path searching.  If
   FILENAME can't be opened, we quit.  */

void
pbm_open_input_file (string filename)
{
  assert (pbm_input_file == NULL);
  
  pbm_input_file = xfopen (filename, "r");
  pbm_input_filename = filename;
}


/* Close the input file.  If it hasn't been opened, we quit.  */

void
pbm_close_input_file ()
{
  assert (pbm_input_file != NULL);
  
  xfclose (pbm_input_file, pbm_input_filename);
  pbm_input_file = NULL;
}



/* Read the header information.
   Modifies the global image_header in main.c.  */

void
pbm_get_header ()
{
  int width, height, format;

  pbm_readpbminit (pbm_input_file, &width, &height, &format);
  image_header.width = (two_bytes) width;
  image_header.height = (two_bytes) height;
  image_header.depth = 0;
  image_header.format = (two_bytes) format;
}



/* Read one scanline of the image.  */

boolean
pbm_get_scanline (one_byte *line_in_bits)
{
  int c = getc (pbm_input_file);
  
  if (c == EOF)
    return false;
    
  ungetc (c, pbm_input_file);
  pbm_readpbmrow (pbm_input_file, line_in_bits, image_header.width, 
		  image_header.format);

  print_scanline (line_in_bits, image_header.width);
  return true;
}
