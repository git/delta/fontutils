/*
# main.h: global variable declarations.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/


#ifndef MAIN_H
#define MAIN_H

#include "types.h"
#include "image-header.h"


/* See main.c for explanations of these globals.  */
extern boolean trace_scanlines;
extern image_header_type image_header;
extern string input_name;
extern string output_name;

/* Generic routines to manipulate the image.  */
extern void (*image_open_input_file) (string filename);
extern void (*image_close_input_file) (void);
extern void (*image_get_header) (void);
extern boolean (*image_get_scanline) (one_byte *);


/* Print a scanline LINE of width WIDTH, if `trace_scanlines' is true..  */
extern void print_scanline (one_byte line[], unsigned width);

#endif /* not MAIN_H */
