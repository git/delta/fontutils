/*
# pbmascii: dump a PBM file (from stdin) on stdout. 
#
# Copyright (C) 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/


#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
  int width, height, format;
  unsigned row;
  unsigned char *image_row;
  
  pbm_readpbminit (stdin, &width, &height, &format);
  
  image_row = malloc (width);
  
  for (row = 0; row < height; row++)
    {
      int c;

      pbm_readpbmrow (stdin, image_row, width, format);
      
      printf ("%5d:", row);
      for (c = 0; c < width; c++)
        putchar (image_row[c] ? '*' : ' ');
      putchar ('\n');
    }
  
  return 0;
}


/*
Local variables:
compile-command: "gcc -g -posix -o pbmascii pbmascii.c pbm.a"
End:
*/
