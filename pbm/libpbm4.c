/*
# libpbm4.c: pbm utility library part 4
#
# Copyright (C) 1988 by Jef Poskanzer.
# Copyright (C) 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/


#include "pbm.h"
#include "libpbm.h"

char
pbm_getc( file )
    FILE* file;
    {
    register int ich;
    register char ch;

    ich = getc( file );
    if ( ich == EOF )
	pm_error( "EOF / read error", 0,0,0,0,0 );
    ch = (char) ich;
    
    if ( ch == '#' )
	{
	do
	    {
	    ich = getc( file );
	    if ( ich == EOF )
		pm_error( "EOF / read error", 0,0,0,0,0 );
	    ch = (char) ich;
	    }
	while ( ch != '\n' );
	}

    return ch;
    }

unsigned char
pbm_getrawbyte( file )
    FILE* file;
    {
    register int iby;

    iby = getc( file );
    if ( iby == EOF )
	pm_error( "EOF / read error", 0,0,0,0,0 );
    return (unsigned char) iby;
    }

int
pbm_getint( file )
    FILE* file;
    {
    register char ch;
    register int i;

    do
	{
	ch = pbm_getc( file );
	}
    while ( ch == ' ' || ch == '\t' || ch == '\n' );

    if ( ch < '0' || ch > '9' )
	pm_error( "junk in file where an integer should be", 0,0,0,0,0 );

    i = 0;
    do
	{
	i = i * 10 + ch - '0';
	ch = pbm_getc( file );
        }
    while ( ch >= '0' && ch <= '9' );

    return i;
    }
