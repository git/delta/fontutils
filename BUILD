This file documents build and installation instructions.

Building fontutils with GCC 4.5.x doesn't require a seperate GCC installation 
but it is recommended that if you are using a GNU/Linux distribution with a 
different version of GCC that you install a new version into a self-contained 
directory. 

You should also have bison, and development libraries of libxt, libxaw, and 
kpathsea installed.

After you have successfully installed the appropriate packages for your platform 
then try the following steps (substituting paths appropriately):

Step #1 - Create configure script:

    autoconf

    NOTE: autoconf will generate the autom4te.cache directory and a configure script.
          These are already present in .cvsignore so you do not need to worry about 
          these being added to the source repository.


Step #2 - Configure your environment:

    CC="/opt/gcc/install/bin/gcc" \ 
    LD_LIBRARY_PATH="/opt/mpc/install/lib:/opt/mpfr/install/lib:/opt/gmp/install/lib" \
    ./configure --prefix=/opt/fontutils/install

    NOTE: The above command assumes you have a custom version of GCC installed as well as 
          custom versions of mpc, mpfr and gmp. If you do not then you can simply execute 
          the configure script. If you want to keep your source directory relatively clean 
          then you should create a directory above the checked out source tree and run all 
          of these steps from that directory and adjusting paths appropriately. 


Step #3 - Build the binaries:

    CC="/opt/gcc/install/bin/gcc" \
    LD_LIBRARY_PATH="/opt/mpc/install/lib:/opt/mpfr/install/lib:/opt/gmp/install/lib" \
    make

    NOTE: You might need to install the xt, xaw, and kpathsea development libraries.  

        If you are using Debian GNU/Linux you can use the following apt command:

        apt-get install libxt-dev libxaw7-dev libkpathsea-dev


        If you are using RHEL you can use the following yum command:

        yum install libXt-devel libXaw-devel kpathsea-devel

    NOTE: We only need kpathsea temporarily until it is completely removed.


Step #4 - Install the binaries:

    CC="/opt/gcc/install/bin/gcc" \
    LD_LIBRARY_PATH="/opt/mpc/install/lib:/opt/mpfr/install/lib:/opt/gmp/install/lib" \
    make install
