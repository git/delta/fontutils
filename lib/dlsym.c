/*
# dlsym.c: Stub interface to dynamic linker routines that SunOS uses but didn't
#          whip with 4.1. 
#
# Copyright (C) 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/


/*
# The C library routine wcstombs in SunOS 4.1 tries to dynamically
# load some routines using the dlsym interface, described in dlsym(3x).
# Unfortunately SunOS 4.1 does not include the necessary library, libdl.
# 
# The R5 Xlib uses wcstombs.  If you link dynamcally, your program can
# run even with the unresolved reference to dlsym.  However, if you
# link statically, you will encounter this bug.  One workaround
# is to include these stub routines when you link.
*/


void *dlopen()
{
    return 0;
}

void *dlsym()
{
    return 0;
}

int dlclose()
{
    return -1;
}
