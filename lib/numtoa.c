/*
# numtoa.c: change numbers back to strings.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#include "config.h"

/* Declarations for these are in global.h.  */

string
itoa (int i)
{
  char a[MAX_INT_LENGTH];
  
  sprintf (a, "%d", i);
  return xstrdup (a);
}

string
utoa (unsigned u)
{
  char a[MAX_INT_LENGTH];
  
  sprintf (a, "%u", u);
  return xstrdup (a);
}

string
xdtoa (double d)
{
  /* I'm not sure if this is really enough, but I also don't know how to
     compute what *would* be enough.  */
  char a[500];
  
  sprintf (a, "%f", d);
  return xstrdup (a);
}
