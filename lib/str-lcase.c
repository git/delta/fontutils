/*
# str-casefold.c: make a string either all uppercase or all lowercase.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#include "config.h"

#include "c-ctype.h"
#include "str-lcase.h"


/* Return a malloced copy of S with all its uppercase letters replaced
   with their lowercase counterparts.  S must not be NULL.  */
   
string
str_to_lower (string s)
{
  unsigned c;
  string lc;
  unsigned length;
  
  assert (s != NULL);

  length = strlen (s);
  lc = xmalloc (length + 1);
  
  for (c = 0; c < length; c++)
    lc[c] = ISUPPER (s[c]) ? TOLOWER (s[c]) : s[c];
    
  lc[length] = 0;
  
  return lc;
  
}
