/*
# logreport.c: showing information to the user.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#include "config.h"

#include "logreport.h"


/* Says whether to output detailed progress reports, i.e., all the data
   on the fitting, as we run.  (-log)  */
boolean logging = false;

FILE *log_file = NULL;


void
flush_log_output ()
{
  if (logging)
    fflush (log_file);
}
