/*
# make-prefix.c: construct a pathname prefix.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#include "config.h"

#include "concatn.h"


string
make_prefix (string prefix, string pathname)
{
  string slash_pos = strrchr (pathname, '/');
  
  if (slash_pos == NULL)
    return concat (prefix, pathname);
  else
    {
      string answer = NULL;
      string path_only = xstrdup (pathname);
      path_only[slash_pos - pathname] = 0;
      answer = concatn (path_only, "/", prefix, slash_pos + 1, NULL);
      free (path_only);
      
      return answer;
    }
}
