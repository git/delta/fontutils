/*
# identity.c:
#
# Copyright (C) 1995, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#include <stdio.h>
#include <stdlib.h>
#include "c-proto.h"
#include "lib.h"
#include "progname.h"

/* Return `hostname:pid' as a string.  */

string
get_identity ()
{
  char s[1024];
  int i;
  
  i = XmuGetHostname (s, sizeof (s));
  if (i == 0)
    FATAL ("limn: Could not get hostname");

  sprintf (&s[i], ":%d", getpid ());
  return (string)xstrdup (s);
}
