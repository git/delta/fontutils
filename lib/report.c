/*
# report.c: showing information to the user online.
#
# Copyright (C) 1992, 2004, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/


#include "config.h"


/* Says whether to output short progress reports as we run.  (-verbose)  */ 
boolean verbose = false;

/* Where to output the reports.  If a particular program uses standard
   output for real output, this gets changed to `stderr'.  */
FILE *report_file = NULL;
