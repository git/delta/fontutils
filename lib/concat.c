/*
# concat.c: dynamic string concatenation.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#include "config.h"

/* Return the concatenation of S1 and S2.  This would be a prime place
   to use varargs.  */

string
concat P2C(const_string, s1, const_string, s2)
{
  string answer = NULL;
  
  assert (s1 != NULL);
  assert (s2 != NULL);

  if (s1 != NULL && s2 != NULL)
    {
      answer = (string) xmalloc (strlen (s1) + strlen (s2) + 1);

      strcpy (answer, s1);
      strcat (answer, s2);
    }

  return answer;
}
