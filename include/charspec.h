/*
# charspec.h: 
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#ifndef CHARSPEC_H
#define CHARSPEC_H

#include "encoding.h"
#include "types.h"


/* If SPEC starts with a digit, return the result of `xparse_charcode
   (SPEC)'.  Otherwise, if ENC is NULL and SPEC is exactly one character
   long, return that character.  Otherwise (ENC is non-NULL), look up
   SPEC as a character name in ENC and return the corresponding character
   code.  If SPEC is NULL, the empty string, an unrecognized name, or
   otherwise invalid, give a fatal error.  */
extern charcode_type xparse_charspec (string spec, encoding_info_type *enc);

#endif /* not CHARSPEC_H */
