/*
# report.h: status reporting routines.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#ifndef REPORT_H
#define REPORT_H

#include <stdio.h>
#include "types.h"



/* The file we write information to.  */
extern FILE *report_file;

/* Only print anything if this is true.  */
extern boolean verbose;


#define REPORT(s)							\
  do if (verbose) { fputs (s, report_file); fflush (report_file); }	\
  while (0)

#define REPORT1(s, e)							\
  do if (verbose) { fprintf (report_file, s, e); fflush (report_file); }\
  while (0)

#define REPORT2(s, e1, e2)						\
  do if (verbose) { fprintf (report_file, s, e1, e2);			\
                    fflush (report_file); }				\
  while (0)

#define REPORT3(s, e1, e2, e3)						\
  do if (verbose) { fprintf (report_file, s, e1, e2, e3);		\
                    fflush (report_file); }				\
  while (0)

#define REPORT4(s, e1, e2, e3, e4)					\
  do if (verbose) { fprintf (report_file, s, e1, e2, e3, e4);		\
		    fflush (report_file); } 				\
  while (0)

#endif /* not REPORT_H */
