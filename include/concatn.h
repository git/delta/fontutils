/*
# concatn.h: concatenate a variable number of strings.
#
# Copyright (C) 1993, 1996, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/

/*
# This is a separate include file only because I don't see the point of
# having every source file include <c-vararg.h>.  The declarations for
# the other concat routines are in lib.h.
*/

#ifndef KPATHSEA_CONCATN_H
#define KPATHSEA_CONCATN_H

#include "c-proto.h"
#include "c-vararg.h"
#include "types.h"

/* Concatenate a null-terminated list of strings and return the result
   in malloc-allocated memory.  */
extern string concatn PVAR1H(const_string str1);

#endif /* not KPATHSEA_CONCATN_H */
