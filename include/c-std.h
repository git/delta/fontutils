/*
# c-std.h: the first header files.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#ifndef C_STD_H
#define C_STD_H

/* Header files that essentially all of our sources need, and
   that all implementations have.  We include these first, to help with
   NULL being defined multiple times.  */
#include <math.h>
#include <stdio.h>

/* POSIX.1 says that <unistd.h> may require <sys/types.h>.  */
#include <sys/types.h>

/* This is the symbol that X uses to determine if <sys/types.h> has been
   read, so we define it.  */
#define __TYPES__

/* And X uses this symbol to say whether we have <stddef.h> etc.  */
#ifndef STDC_HEADERS
#define X_NOT_STDC_ENV
#endif

/* Be sure we have constants from <unistd.h>.  */
#include "c-unistd.h"

#if STDC_HEADERS
#include <stdlib.h>
/* Include <stdlib.h> first to help avoid NULL redefinitions on some
   systems.  Now that we no longer include <stdlib.h> it may be
   irrelevant.  */
#else
extern char *getenv ();
#ifndef ALLOC_RETURN_TYPE
#ifdef DOS
#define ALLOC_RETURN_TYPE void
#else
#define ALLOC_RETURN_TYPE char
#endif /* not DOS */
#endif /* not ALLOC_RETURN_TYPE */
extern ALLOC_RETURN_TYPE *calloc (), *malloc (), *realloc ();
#endif /* not STDC_HEADERS */

/* strchr vs. index, memcpy vs. bcopy, etc.  */
#include "c-memstr.h"

/* Error numbers and errno declaration.  */
#include "c-errno.h"

/* Numeric minima and maxima.  */
#include "c-minmax.h"

/* popen is part of POSIX.2, not POSIX.1.  So STDC_HEADERS isn't enough.  */
extern FILE *popen ();
extern double hypot ();

#endif /* not C_STD_H */
