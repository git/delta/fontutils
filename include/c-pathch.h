/*
# c-pathch.h: define the characters which separate components of
#             pathnames and environment variable paths.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#ifndef C_PATHCH_H
#define C_PATHCH_H

/* What separates pathname components?  */
#ifndef PATH_SEP
#ifdef VMS
#define PATH_SEP ':'
#define PATH_SEP_STRING ":"
#else
#ifdef DOS
#define PATH_SEP '\\'
#define PATH_SEP_STRING "\\"
/* On DOS, it's good to allow both \ and / between directories.  */
#define IS_PATH_SEP(ch) ((ch) == PATH_SEP || (ch) == '/')
#else
#ifdef VMCMS
#define PATH_SEP ' '
#define PATH_SEP_STRING " "
#else
#define PATH_SEP '/'
#define PATH_SEP_STRING "/"
#endif /* not VM/CMS */
#endif /* not DOS */
#endif /* not VMS */
#endif /* not PATH_SEP */

#define DIR_SEP ':'
#ifndef IS_DIR_SEP
#define IS_DIR_SEP(ch) ((ch) == DIR_SEP)
#endif

#ifndef IS_PATH_SEP
#define IS_PATH_SEP(ch) ((ch) == PATH_SEP)
#endif

/* What separates elements in environment variable path lists?  */
#ifndef PATH_DELIMITER
#ifdef VMS
#define PATH_DELIMITER ','
#define PATH_DELIMITER_STRING ","
#else
#ifdef DOS
#define PATH_DELIMITER ';'
#define PATH_DELIMITER_STRING ";"
#else
#ifdef VMCMS
#define PATH_DELIMITER ' '
#define PATH_DELIMITER_STRING " "
#else
#define PATH_DELIMITER ':'
#define PATH_DELIMITER_STRING ":"
#endif /* not VM/CMS */
#endif /* not DOS */
#endif /* not VMS */
#endif /* not PATH_DELIMITER */

#endif /* not C_PATHCH_H */
