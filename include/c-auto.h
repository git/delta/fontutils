/* include/c-auto.h.  Generated from c-auto.h.in by configure.  */
/* c-auto.h.in.  Generated automatically from configure.in by autoheader.  */

/* Define to empty if the keyword does not work.  */
/* #undef const */

/* Define if you have dirent.h.  */
/* #undef DIRENT */

/* Define if you don't have dirent.h, but have ndir.h.  */
/* #undef NDIR */

/* Define if you need to in order for stat and other things to work.  */
/* #undef _POSIX_SOURCE */

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Define if you don't have dirent.h, but have sys/dir.h.  */
/* #undef SYSDIR */

/* Define if you don't have dirent.h, but have sys/ndir.h.  */
/* #undef SYSNDIR */

/* Define if the closedir function returns void instead of int.  */
/* #undef VOID_CLOSEDIR */

/* Define if your putenv doesn't waste space when the same environment
   variable is assigned more than once, with different (malloced)
   values.  This is true only on NetBSD/FreeBSD, as far as I know. See
   xputenv.c.  */
/* #undef SMART_PUTENV */

/* Define if you have memmove.  */
/* #undef HAVE_MEMMOVE */

/* Define if you have putenv.  */
/* #undef HAVE_PUTENV */

/* Define if you have the <assert.h> header file.  */
#define HAVE_ASSERT_H 1

/* Define if you have the <float.h> header file.  */
#define HAVE_FLOAT_H 1

/* Define if you have the <limits.h> header file.  */
#define HAVE_LIMITS_H 1

/* Define if you have the <memory.h> header file.  */
#define HAVE_MEMORY_H 1

/* Define if you have the <pwd.h> header file.  */
#define HAVE_PWD_H 1

/* Define if you have the <stdlib.h> header file.  */
#define HAVE_STDLIB_H 1

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H 1

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H 1

/* Define if you have the dnet library (-ldnet).  */
/* #undef HAVE_LIBDNET */

/* Define if you have the dnet_stub library (-ldnet_stub).  */
/* #undef HAVE_LIBDNET_STUB */

/* Define if you have the socket library (-lsocket).  */
/* #undef HAVE_LIBSOCKET */
