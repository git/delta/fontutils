/*
# config.h: master configuration file, included first by all compilable
#           source files (not headers).
#
# Copyright (C) 1992, 2004, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#ifndef CONFIG_H
#define CONFIG_H

/* Standard definitions.  */
#include "kpathsea-config.h"

/* System dependencies that are figured out by `configure'.  */
#include "c-auto.h"

/* ``Standard'' headers.  */
#include "c-std.h"

/* Standard in ANSI C.  */
#include <assert.h>

/* Usually declared in <math.h>, but not always.  */
#ifndef M_PI
#define M_PI	3.14159265358979323846
#endif

/* Macros to discard or keep prototypes.  */
#include "c-proto.h"

/* Some definitions of our own.  */
#include "global.h"

/* undo kpathsea */
#undef fopen
#undef fclose
#undef KPSE_DEBUG_P
#define KPSE_DEBUG_p(a) 0

#endif /* not CONFIG_H */
