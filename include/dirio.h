/*
# dirio.h: checked directory operations.
#
# Copyright (C) 1992, 2011 Free Software Foundation, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
*/



#ifndef DIRIO_H
#define DIRIO_H

#if DIRENT || _POSIX_VERSION
#include <dirent.h>
#define NLENGTH(dirent) strlen ((dirent)->d_name)
#else /* not (DIRENT or _POSIX_VERSION) */
#define dirent direct
#define NLENGTH(dirent) ((dirent)->d_namlen)

#ifdef SYSNDIR
#include <sys/ndir.h>
#endif

#ifdef NDIR
#include <ndir.h>
#endif

#ifdef SYSDIR
#include <sys/dir.h>
#endif

#endif /* not (DIRENT or _POSIX_VERSION) */

/* Like opendir, closedir, and chdir, but abort if DIRNAME can't be opened.  */
extern DIR *xopendir P1H(string dirname);
extern void xclosedir P1H(DIR *);

#if 0
/* Returns true if FN is a directory (or a symlink to a directory).  */
extern boolean dir_p P1H(string fn);
#endif

/* Returns true if FN is directory with no subdirectories.  */
extern boolean leaf_dir_p P1H(string fn);

#endif /* not DIRIO_H */
